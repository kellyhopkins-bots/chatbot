#!/usr/bin/env python3
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os

""" Bot Configuration """


class DefaultConfig:
    """ Bot Configuration """

    PORT = 3978
    APP_ID = os.environ.get("MicrosoftAppId", "")
    APP_PASSWORD = os.environ.get("MicrosoftAppPassword", "")

    QNA_KNOWLEDGEBASE_ID = "14a2ec31-7ba5-4ea9-b37c-7aa02c803402" #os.environ.get("QnAKnowledgeBaseID", "")
    QNA_ENDPOINT_HOST = "https://mbda-qna-bot.azurewebsites.net/qnamaker" #os.environ.get("QnAEndpoint", "")
    QNA_ENDPOINT_KEY = "5e2bd5e5-71f9-4375-a3ab-2dbff3c81ace"

    LUIS_APP_ID = "ab744ed0-1d87-4160-ad36-744b3cea5396" #os.environ.get("LuisAppId", "")
    LUIS_API_KEY = "0bdf7219cc9541df8c77adb543ac0b48" #os.environ.get("LuisAPIKey", "")
    # LUIS endpoint host name, ie "westus.api.cognitive.microsoft.com"
    LUIS_API_HOST_NAME = "westus.api.cognitive.microsoft.com" #os.environ.get("LuisAPIHostName", "")

    COSMOS_DB_ENDPOINT = "https://commerce-chatbot.documents.azure.com:443/"
    COSMOS_DB_AUTH_KEY="pWqwHgaiw9bDdoQrewZYjCO0VKtFSeYsAvinfYl7lk0AlorShzxIFDDc5G9Pkp2VaKMArR99PVCWiZHxpfHI3Q=="
    COSMOS_DB_DATABASE_ID="chatbot-test"
    COSMOS_DB_CONTAINER_ID="chatbot-test"

    password = "HpP9TmLDfSrB8Smj"
