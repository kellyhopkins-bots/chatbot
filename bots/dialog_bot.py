# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

from botbuilder.core import ActivityHandler, ConversationState, UserState, TurnContext, StoreItem, MemoryStorage
from botbuilder.dialogs import Dialog
from helpers.dialog_helper import DialogHelper
from botbuilder.azure import CosmosDbPartitionedStorage, CosmosDbPartitionedConfig, BlobStorage, BlobStorageSettings

class UtteranceLog(StoreItem):
    """
    Class for storing a log of utterances (text of messages) as a list.
    """

    def __init__(self):
        super(UtteranceLog, self).__init__()
        self.utterance_list = []
        self.turn_number = 0
        self.e_tag = "*"


class DialogBot(ActivityHandler):
    def __init__(
        self,
        conversation_state: ConversationState,
        user_state: UserState,
        dialog: Dialog,
    ):
        if conversation_state is None:
            raise Exception(
                "[DialogBot]: Missing parameter. conversation_state is required"
            )
        if user_state is None:
            raise Exception("[DialogBot]: Missing parameter. user_state is required")
        if dialog is None:
            raise Exception("[DialogBot]: Missing parameter. dialog is required")

        self.conversation_state = conversation_state
        self.user_state = user_state
        self.dialog = dialog
        #self.storage = MemoryStorage()

        cosmos_config = CosmosDbPartitionedConfig(
            cosmos_db_endpoint="https://commerce-chatbot.documents.azure.com:443/",
            auth_key="pWqwHgaiw9bDdoQrewZYjCO0VKtFSeYsAvinfYl7lk0AlorShzxIFDDc5G9Pkp2VaKMArR99PVCWiZHxpfHI3Q==",
            database_id="chatbot-test",
            container_id = "chatbot-test",
            compatibility_mode = False
        )
        self.storage = CosmosDbPartitionedStorage(cosmos_config)

        # blob_settings = BlobStorageSettings(
        #     container_name="chatbotstorage",
        #     connection_string="DefaultEndpointsProtocol=https;AccountName=chatbotstorage;AccountKey=U4Mc8XItd2BG7ukXzSwWDVTjtgzy7NWVeX6WeF4NtASvy2f3oMOI3S12jfHC50aBFKp0smUzlAbf/xESzNPRkg==;EndpointSuffix=core.windows.net"
        # )
        # self.storage = BlobStorage(blob_settings)

    async def on_turn(self, turn_context: TurnContext):
        await super().on_turn(turn_context)

        # Save any state changes that might have occurred during the turn.
        await self.conversation_state.save_changes(turn_context, False)
        await self.user_state.save_changes(turn_context, False)

    async def on_message_activity(self, turn_context: TurnContext):
        utterance = turn_context.activity.text

        # read the state object
        store_items = await self.storage.read(["UtteranceLog"])

        if "UtteranceLog" not in store_items:
            # add the utterance to a new state object.
            utterance_log = UtteranceLog()
            utterance_log.utterance_list.append(utterance)
            utterance_log.turn_number = 1
        else:
            # add new message to list of messages existing state object.
            utterance_log: UtteranceLog = store_items["UtteranceLog"]
            utterance_log.utterance_list.append(utterance)
            utterance_log.turn_number = utterance_log.turn_number + 1
            #print(utterance_log)
        
        try:
            # Save the user message to your Storage.
            changes = {"UtteranceLog": utterance_log}
            await self.storage.write(changes)
        except Exception as exception:
            # Inform the user an error occurred.
            print("save error")


        await DialogHelper.run_dialog(
            self.dialog,
            turn_context,
            self.conversation_state.create_property("DialogState"),
        )
