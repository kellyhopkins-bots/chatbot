from botbuilder.core import MessageFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt

class SuggestionDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(SuggestionDialog, self).__init__(dialog_id or SuggestionDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(
            WaterfallDialog("WFDialog", [self.first_step, self.second_step],)
        )
        self.initial_dialog_id = "WFDialog"

    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        prompt_options = PromptOptions(
            prompt=MessageFactory.text("Was there anything I wasn't able to answer?")
        )
        return await step_context.prompt(TextPrompt.__name__, prompt_options)
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        user = step_context.result
        await step_context.context.send_activity(
            MessageFactory.text(f"Thanks for the feedback! I've noted your response as '{user}'.")
        )
        return await step_context.end_dialog()