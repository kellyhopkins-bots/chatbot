from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from data_models import BusinessProfile

from services import Qna

from dialogs.qna_dialog import QnaDialog
from dialogs.data_dialog import DataDialog

CARD_PROMPT = "cardPrompt"

class FinancialDialog(ComponentDialog):
    def __init__(self, user_state: UserState): #, dialog_id: str = None):
        #super(FinancialDialog, self).__init__(dialog_id or FinancialDialog.__name__)
        super(FinancialDialog, self).__init__(FinancialDialog.__name__)

        self.user_state = user_state

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.BUSINESS_INFO = "value-userInfo"
        
        self.add_dialog(QnaDialog(QnaDialog.__name__))
        self.add_dialog(DataDialog(DataDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        await step_context.context.send_activity(MessageFactory.text(
            "First I need to ask you some questions"
        ))
        return await step_context.begin_dialog(DataDialog.__name__)
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        business_info: BusinessProfile = step_context.result

        #print(self.user_state)
        print(business_info)

        accessor = self.user_state.create_property("BusinessProfile")
        await accessor.set(step_context.context, business_info)

        return await step_context.end_dialog()
