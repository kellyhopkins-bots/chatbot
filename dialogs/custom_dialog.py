from botbuilder.core import MessageFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt

from data_models import UserDetails
from dialogs.review_selection_dialog import ReviewSelectionDialog

class CustomDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(CustomDialog, self).__init__(dialog_id or CustomDialog.__name__)

        self.USER_INFO = "value-userInfo"

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))

        self.add_dialog(
            WaterfallDialog(
                "WFDialog",
                [
                    self.name_step,
                    self.age_step,
                    self.acknowledgement_step
                ],
            )
        )

        self.initial_dialog_id = "WFDialog"
    
    async def name_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.values[self.USER_INFO] = UserDetails()
        
        prompt_options = PromptOptions(
            prompt=MessageFactory.text("Please enter your name.")
        )
        return await step_context.prompt(TextPrompt.__name__, prompt_options)
    
    async def age_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        user_details = step_context.values[self.USER_INFO]
        user_details.name = step_context.result

        prompt_options = PromptOptions(
            prompt = MessageFactory.text("Please enter your age.")
        )
        return await step_context.prompt(NumberPrompt.__name__, prompt_options)
    
    async def acknowledgement_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        user_details: UserDetails = step_context.values[self.USER_INFO]
        
        await step_context.context.send_activity(
            MessageFactory.text(f"Thanks for participating, {user_details.name}.")
        )

        

        return await step_context.end_dialog(user_details)


