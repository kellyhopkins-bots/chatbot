import os
from botbuilder.core import MessageFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from data_models import UserProfile
from dialogs.info_dialog import InfoDialog
from dialogs.business_dialog import BusinessDialog
from dialogs.data_dialog import DataDialog
from dialogs.financial_dialog import FinancialDialog

from services import Qna, Luis, readfile, deletefile

CARD_PROMPT = "cardPrompt"

class LuisDialog(ComponentDialog):
    def __init__(self, user_state: UserState, dialog_id: str = None):
        super(LuisDialog, self).__init__(dialog_id or LuisDialog.__name__)

        self.user_state = user_state

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.add_dialog(InfoDialog(InfoDialog.__name__))
        self.add_dialog(BusinessDialog(BusinessDialog.__name__))
        self.add_dialog(DataDialog(DataDialog.__name__))
        self.add_dialog(FinancialDialog(FinancialDialog.__name__))

        self.add_dialog(
            WaterfallDialog(
                "WFDialog",
                [
                    self.luis_step,
                    self.intent_step,
                    self.last_step
                ],
            )
        )

        self.initial_dialog_id = "WFDialog"

    async def luis_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # prompt_options = PromptOptions(
        #     prompt=MessageFactory.text("What can I help you with?")
        # )
        # return await step_context.prompt(TextPrompt.__name__, prompt_options) 

        if os.path.exists('data/text.txt'):
            f = readfile()
            deletefile()
            return await step_context.next(f)
        else:
            return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text(
                    "What card would you like to see? You can click or type the card name"
                ),
                retry_prompt=MessageFactory.text(
                    "That was not a valid choice, please select a card or number from 1 "
                    "to 9."
                ),
                choices=self.get_choices(),
            ),
        )


        # return await step_context.prompt(
        #     CARD_PROMPT,
        #     PromptOptions(
        #         prompt=MessageFactory.text(
        #             "What card would you like to see? You can click or type the card name"
        #         ),
        #         retry_prompt=MessageFactory.text(
        #             "That was not a valid choice, please select a card or number from 1 "
        #             "to 9."
        #         ),
        #         choices=self.get_choices(),
        #     ),
        # )

    async def intent_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        try:
            request = step_context.result.value
        except:
            request = step_context.result
        
        response = Luis(query=request)
        intent = response['topIntent']
        #pred = response['prediction']
        #intent = pred['topIntent']

        await step_context.context.send_activity(
            MessageFactory.text(f"Luis intent: {intent}")
        )
        
        if intent == "Weather.CheckWeatherValue":
            message = "TODO: weather flow here"
        elif intent == "GeneralInformation":
            return await step_context.begin_dialog(InfoDialog.__name__)
        elif intent == "BusinessCenter":
            return await step_context.begin_dialog(BusinessDialog.__name__)
        elif intent == "FinancialResources":
            return await step_context.begin_dialog(FinancialDialog.__name__)
        else:
            message = "Unknown Intent"
            return await step_context.replace_dialog(LuisDialog.__name__)

        # await step_context.context.send_activity(
        #     MessageFactory.text(message)
        # )

        #return await step_context.next([])

        
    async def last_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        message = "End of dialog."

        await step_context.context.send_activity(
            MessageFactory.text(message)
        )
        return await step_context.end_dialog()  
    

    def get_choices(self):
        card_options = [
            Choice(value="General Information", synonyms=["1"]),
            Choice(value="Financial Resources", synonyms=["2"]),
            Choice(value="Strategic Assistance", synonyms=["3"]),
            Choice(value="Business Center Experts", synonyms=["4"]),
            Choice(value="Emergency Assistance", synonyms=["5"]),
        ]

        return card_options