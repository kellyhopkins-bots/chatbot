from botbuilder.core import MessageFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt

from data_models import UserProfile
from dialogs.review_selection_dialog import ReviewSelectionDialog

class WeatherDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(WeatherDialog, self).__init__(dialog_id or WeatherDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(
            WaterfallDialog(
                "WFDialog",
                [
                    self.confirmation_step
                ]
            )
        )

        self.initial_dialog_id = "WFDialog"
    
    async def confirmation_step(parameter_list):
        pass