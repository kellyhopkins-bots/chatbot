from botbuilder.core import MessageFactory, CardFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from services import Qna

from dialogs.qna_dialog import QnaDialog

CARD_PROMPT = "cardPrompt"

class InfoDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(InfoDialog, self).__init__(dialog_id or InfoDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))
        
        self.add_dialog(QnaDialog(QnaDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step, self.fourth_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text(
                    "What card would you like to see? You can click or type the card name"
                ),
                retry_prompt=MessageFactory.text(
                    "That was not a valid choice, please select a card or number from 1 "
                    "to 9."
                ),
                choices=self.get_choices(),
            ),
        )
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        #await step_context.context.send_activity(MessageFactory.text(f"You said: {step_context.result.value}"))
        response = step_context.result.value

        if response == "MBDA":
            return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text(
                    "What card would you like to see? You can click or type the card name"
                ),
                # retry_prompt=MessageFactory.text(
                #     "That was not a valid choice, please select a card or number from 1 "
                #     "to 9."
                # ),
                choices= [
                    Choice(value="What is MBDA?"),
                    Choice(value="Who Does MBDA Serve?"),
                    Choice(value="What is MBDA's objective?"),
                    Choice(value="Other"),
                    Choice(value="Sample Question"),
                ]
            ),
            )
        elif response == "MBEs":
            return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text(
                    "What card would you like to see? You can click or type the card name"
                ),
                retry_prompt=MessageFactory.text(
                    "That was not a valid choice, please select a card or number from 1 "
                    "to 9."
                ),
                choices= [
                    Choice(value="What Qualifies as a Minority Business Enterprise?"),
                    Choice(value="What MBDA resources are available?"),
                    Choice(value="Other"),
                ]
            ),
            )
        elif response == "General Topics":
            return await step_context.prompt(
            CARD_PROMPT,
            PromptOptions(
                prompt=MessageFactory.text(
                    "What card would you like to see? You can click or type the card name"
                ),
                retry_prompt=MessageFactory.text(
                    "That was not a valid choice, please select a card or number from 1 "
                    "to 9."
                ),
                choices= [
                    Choice(value="MBDA Programs"),
                    Choice(value="Other"),
                ]
            ),
            )

        #return await step_context.end_dialog()
    
    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        response = step_context.result.value

        #await step_context.context.send_activity(MessageFactory.text(f"You said: {response}"))

        if response == "Other":
            prompt_options = PromptOptions(
            prompt=MessageFactory.text("Type your question below:")
            )
            return await step_context.prompt(TextPrompt.__name__, prompt_options) 
        else:
            res = Qna(query=response)

            # if len(res['context']['prompts']) > 0:
            #     return await step_context.prompt(
            #     CARD_PROMPT,
            #     PromptOptions(
            #         prompt=MessageFactory.text(
            #             f"Qna Answer: {res['answer']}"
            #         ),
            #         retry_prompt=MessageFactory.text(
            #             "That was not a valid choice, please select a card or number from 1 "
            #             "to 9."
            #         ),
            #         choices= [
            #             i['displayText'] for i in res['context']['prompts']
            #         ]
            #     ),
            #     )
            # else:
            #     await step_context.context.send_activity(MessageFactory.text(f"Qna Answer: {res['answer']}"))
            #     return await step_context.end_dialog()

            await step_context.context.send_activity(MessageFactory.text(f"Qna Answer: {res['answer']}"))
            return await step_context.end_dialog()
    
    async def fourth_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        response = step_context.result
        res = Qna(query=response)
        await step_context.context.send_activity(MessageFactory.text(f"Qna Answer: {res['answer']}"))

        return await step_context.end_dialog()
    


    def get_choices(self):
        card_options = [
            Choice(value="MBDA", synonyms=["adaptive"]),
            Choice(value="MBEs", synonyms=["animation"]),
            Choice(value="MBDA Readiness", synonyms=["audio"]),
            Choice(value="General Topics", synonyms=["audio"]),
        ]

        return card_options