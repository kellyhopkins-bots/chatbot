from botbuilder.core import MessageFactory, CardFactory, UserState
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from data_models import BusinessProfile

from services import Qna

from dialogs.qna_dialog import QnaDialog

CARD_PROMPT = "cardPrompt"

class DataDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(DataDialog, self).__init__(dialog_id or DataDialog.__name__)

        # self.user_profile_accessor = user_state.create_property("UserProfile ")

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.BUSINESS_INFO = "value-userInfo"
        
        self.add_dialog(QnaDialog(QnaDialog.__name__))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step, self.third_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.values[self.BUSINESS_INFO] = BusinessProfile()

        return await step_context.prompt(TextPrompt.__name__, PromptOptions(
            prompt=MessageFactory.text("Enter business name:")
        ))
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        business_profile = step_context.values[self.BUSINESS_INFO]
        business_profile.name = step_context.result

        return await step_context.prompt(NumberPrompt.__name__, PromptOptions(
            prompt=MessageFactory.text("Enter revenue:")
        ))

    async def third_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        business_profile = step_context.values[self.BUSINESS_INFO]
        business_profile.rev = step_context.result

        print(step_context.values)

        return await step_context.end_dialog(business_profile)