from botbuilder.core import MessageFactory, CardFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext,
    ComponentDialog,
)
from botbuilder.dialogs.prompts import PromptOptions, TextPrompt, NumberPrompt, ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import ChannelAccount, CardAction, ActionTypes, SuggestedActions

from services import Qna

CARD_PROMPT = "cardPrompt"

class QnaDialog(ComponentDialog):
    def __init__(self, dialog_id: str = None):
        super(QnaDialog, self).__init__(dialog_id or QnaDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(CARD_PROMPT))

        self.add_dialog(WaterfallDialog("WFDialog", [self.first_step, self.second_step]))

        self.initial_dialog_id = "WFDialog"
    
    async def first_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        prompt_options = PromptOptions(
            prompt=MessageFactory.text("Type your question below:")
        )
        return await step_context.prompt(TextPrompt.__name__, prompt_options)
    
    async def second_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        request = step_context.result
        res = Qna(query=request)
        return await step_context.end_dialog[res]