from typing import List

class UserDetails:
    def __init__(
        self, name: str = None, age: int = 0
    ):
        self.name: str = name
        self.age: int = age