import numpy as np
import pandas as pd

import re

import requests
import certifi
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

session = requests.Session()
session.verify = False

data = pd.read_csv('data/business_center.csv')
df = pd.DataFrame(columns=data.columns)

endpoint = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=us-zip-code-latitude-and-longitude&q={zipcode}&facet=state&facet=timezone&facet=dst"

data['Coordinates'] = session.get(f"https://public.opendatasoft.com/api/records/1.0/search/?dataset=us-zip-code-latitude-and-longitude&q={data['5-Digit Zip']}&facet=state&facet=timezone&facet=dst").json()

data.to_csv('data/bc.csv')

# for i,r in data.iterrows():
#     res = session.get(f"https://public.opendatasoft.com/api/records/1.0/search/?dataset=us-zip-code-latitude-and-longitude&q={r['Zip']}&facet=state&facet=timezone&facet=dst").json()

#     try:
#         r['Coordinates'] = res['records'][0]['fields']['geopoint']
#         df.append(r)
#     except:
#         print("...")

# df.to_csv('data/bc.csv')